<x-layout>

<div class="container-fluid bgCustom1">
  <div class="row justify-content-center bgCustom1 mt-5">
    <div class="col-12 col-lg-6 mt-5 p-3 shadow">
      <div class="row bg-white p-5">
        <div class="col-6">
         <form action="{{route('mail.send')}}" method="post">
          @csrf
          <div class="mb-3">
            <label for="exampleInputEmail1"  class="form-label">Indirizzo Email</label>
            <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="exampleInputName"  class="form-label">Nome</label>
            <input type="text" class="form-control" name="name" id="exampleInputName" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1"  class="form-label">Presentati</label>
            <textarea class="form-control" name="description" col='30' row='15' id="exampleInputEmail1" aria-describedby="emailHelp"></textarea>
          </div>

            <button type='submit' class='btn btn-dark brgold' > Invia</button>

        </form>
        </div>
       </div>
      </div>
    </div>
</div>


















</x-layout>