<nav id="navbar"  class="navbar navbar-expand-lg navbar-dark bgcustom container-fluid fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ route('homepage') }}"><img src="/media/w1.png" class="logonav iconacustom" alt="logo presto"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link {{Route::is('homepage') ? 'txtcolor' : '' }} hovergold" aria-current="page" href="{{ route('homepage') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link hovergold {{Route::is('announcement.index') ? 'txtcolor' : '' }}" href="{{ route('announcement.index') }}">{{__('ui.nav1')}}</a>
        </li>

       <li class="nav-item">
        <x-locale lang="it" nation="it"></x-locale>
       </li>

       <li class="nav-item">
        <x-locale lang="en" nation="gb"></x-locale>
      </li>

       <li class="nav-item">
        <x-locale lang="es" nation="es"></x-locale>
      </li>

    @if(Auth::user() && !Auth::user()->is_revisor)
        <li class="nav-item">
          <a class="nav-link hovergold" href="{{route('formMail')}}">{{__('ui.nav5')}}</a>
        </li>
    @endif

        @guest
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle hovergold" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            {{__('ui.nav3')}}
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="{{route('register')}}">Register</a></li>
            <li><a class="dropdown-item" href="{{route('login')}}">Login</a></li>
          </ul>
        </li>



  
        @else
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle hovergold" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
           {{__('ui.nav2')}}, {{Auth::user()->name}}
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="" onclick="event.preventDefault(); document.querySelector('#form-logout').submit(); ">Logout</a></li>
          </ul>
            <form method="post" action="{{route('logout')}}" id="form-logout" class="d-none">@csrf</form>
        </li>
        @endguest

        {{-- tasto revisor con notifica annunci --}}
        @if(Auth::user() && Auth::user()->is_revisor)
        <li class="nav-item">
          <a href="{{route('revisor.index')}}" class="nav-link hovergold {{Route::is('revisor.index') ? 'txtcolor' : '' }}">
          Home {{__('ui.nav4')}}
          <span class="badge badge-pill badge-warning txtcolor">
            {{\App\Models\Announcement::ToBeRevisionedCount()}}
          </span>
          </a>
        </li>
      @endif

      </ul>

            @guest
                <a href="{{route('login')}}" class="btn btn-dark brgold">{{__('ui.addAdv')}}</a>
            @else
                <a href="{{route('announcement.create')}}" class="btn btn-dark brgold">{{__('ui.addAdv')}}</a>
            @endguest
    
    </div>
  </div>
</nav>