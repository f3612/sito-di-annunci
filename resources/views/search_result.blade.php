<x-layout>

    <div class="container">
        <div class="row">
            <div class="col-12 my-5 p-5 shadow">
                <h1 class='text-center'>
                   {{__('ui.ricerca')}}: <strong class="txtcolor"> {{$q}} </strong>
                </h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            
        @foreach ($announcements as $announcement)
            <div class="col-12 col-lg-4 my-3">
                
                <div class="card shadow" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title fw-bold">{{$announcement->name}}</h5>
                        <p class="prezzo">{{$announcement->price}}</p>

                        @foreach($announcement->images as $image)
                        @if($loop->first)
                          <img id="card-first-img3" src="{{$image->getUrl(300,300)}}"
                          class="card-img-top" alt="immagine prodotto">
                  
                        @endif
                        @if($loop->last)
                          <img id="card-last-img3" src="{{$image->getUrl(300,300)}}"
                          class="card-img-top" alt="immagine prodotto">
                  
                        @endif
                      @endforeach
                    
                        
                        
                        <p class="card-text">{{$announcement->description}}</p>
                        <p>{{__('ui.inseritoil')}}: {{$announcement->created_at->format('d/m/y')}}</p>
                        <strong> {{__('ui.category')}}: <a href="{{route('announcements.category', [
                            $announcement->category->name,
                            $announcement->category->id,
                            ])}}"> {{ $announcement->category->name}}</a></strong>
                        <div class="card-footer">
                          <p>{{__('ui.aggiuntoda')}}: {{ $announcement->user->name }}</p>
                          <a href="{{ route('detailad',compact('announcement')) }}" class="btn btn-dark brgold">{{__('ui.scopri')}}</a>
                        </div>
                    </div>
                </div>
        
                  
            </div>
            @endforeach 
        </div>
    </div>



</x-layout>