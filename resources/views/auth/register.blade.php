<x-layout>


      <div class="container-fluid mt-5">
            <div class="row  justify-content-center bgCustom1">
              <div class="col-12 col-lg-6 shadow p-2 mt-5">
                <div class="row bg-white p-5 ">
                  <div class="col-6">
                    <form method="POST" action="{{route('register')}}">
                        @csrf
                      <div class="mb-3">
                        <label for="exampleInputName" class="form-label">Username</label>
                        <input type="text" class="form-control"  name="name" id="exampleInputName" aria-describedby="emailHelp">
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputEmail1"  class="form-label">Email address</label>
                        <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputPassword" class="form-label">Password</label>
                        <input type="password" class="form-control" name="password" id="exampleInputPassword">
                      </div>
                      <div class="mb-3">
                        <label for="exampleInputPasswordConfirmation" class="form-label"> {{__('ui.conferma')}} Password</label>
                        <input type="password" class="form-control" name="password_confirmation" id="exampleInputPasswordConfirmation">
                      </div>
                      
                      <button type="submit" class="btn btn-dark brgold">{{__('ui.registrati')}}</button>
                    </form>
                  
                  </div>
                  <div class="col-6 d-flex justify-content-center align-items-center" >
                    <img src="/media/fotocamere.jpg" class="img-fluid" alt="" srcset="">
                  </div>
                </div>

              </div>
            </div>
      </div>




</x-layout>