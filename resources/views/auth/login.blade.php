<x-layout>

  <div class="mt-5 p-3">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
      @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
      @endforeach
            </ul>
        </div>
      @endif

  </div>


<div class="container-fluid bgCustom1">
  <div class="row justify-content-center bgCustom1">
    <div class="col-12 col-lg-6 mt-5 p-3 shadow">
      <div class="row bg-white p-5">
        <div class="col-6">
          <form action="{{route('login')}}" method="post">
          @csrf
          <div class="mb-3">
            <label for="exampleInputEmail1"  class="form-label">Email address</label>
            <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword" class="form-label">Password</label>
            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="exampleInputPassword" >
            
          </div>
          
          
          <button type="submit" class="btn btn-dark brgold">Login</button>
          </form>

          <a href="{{route('register')}}"class="mt-2 text-decoration-none mt-2">{{__('ui.cliccaqui')}}</a>
        </div>
        <div class="col-6 d-flex justify-content-center align-items-center">
          <img src="/media/fotocamere.jpg" alt="" class="img-fluid" srcset="">
        </div>
      </div>

      
      
    </div>
  </div>
</div>

</x-layout>