<x-layout>

@if($announcement)
<div class="container-fluid my-5">
    <div class="row my-5 p-3">
        <div class="col-12 my-5">
            <div class="card">
                <div class="card-header">
                    {{__('ui.Nannuncio')}}: {{$announcement->id}}
                    
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-4 col-md-2">
                            <h3>{{__('ui.utente')}}</h3>
                        </div>
                        <div class="col-8 col-md-10">
                            {{$announcement->user->id}},
                            {{$announcement->user->name}},
                            {{$announcement->user->email}},

                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-4 col-md-2">
                            <h3>{{__('ui.titolo')}}</h3>
                        </div>
                        <div class="col-8 col-md-10">
                            {{$announcement->name}}
                        </div>
                    </div>
                    
                    <hr>
                       
                    <div class="row">
                        <div class="col-4 col-md-2">
                            <h3>{{__('ui.descr')}}</h3>
                        </div>
                        <div class="col-8 col-md-10">
                            {{$announcement->description}}
                        </div>
                    </div>
                     
                    <hr>

                    <div class="row">
                        <div class="col-4 col-md-2">
                            <h3>Brand</h3>
                        </div>
                        <div class="col-8 col-md-10">
                            {{$announcement->brand}}
                        </div>
                    </div>

                    <hr>

                    <div class="row wrap">
                        
                            <h3>{{__('ui.img')}}</h3>
                        
                        @foreach($announcement->images as $image)
                        <div class="col-12 col-md-4 my-3 ">                        
                              
                            <img src="{{Storage::url($image->file)}}" class=" img-fluid" alt="immagine prodotto">
                        </div>
                            <div class='col-12 col-md-8 my-3'>
                                <p> adult: {{$image->adult}} </p><br>
                                <p> spoof: {{$image->spoof}} </p> <br>
                                <p> medical: {{$image->medical}}</p> <br>
                                <p> violence:{{$image->violence}}</p> <br>
                                <p> racy:{{$image->racy}}</p> <br>

                                <ul>
                                    @if($image->labels)
                                    @foreach($image->labels as $label)
                                    <li>{{$label}}</li>
                                    @endforeach
                                    @endif
                                
                                </ul>

                                
                            
                        </div>
                        
                        @endforeach

                            
                    </div>

                </div>
            </div>
        </div>
    </div>


<div class="row justify-content-center my-5">
    <div class="col-6 d-flex justify-content-between">
        <form action="{{route('revisor.reject', $announcement->id)}}" method="post">@csrf <button class="submit btn btn-danger btn-revisor">{{__('ui.rifiuta')}}</button></form>
        <form action="{{route('revisor.accept', $announcement->id)}}" method="post">@csrf <button class="submit btn btn-success btn-revisor">{{__('ui.accetta')}}</button></form>
    </div>
</div>



</div>

@else

<div class="container mt-5">
    <div class="row">
        <div class="col-12 mt-5">
            <p>{{__('ui.noannunci')}}</p>
        </div>
    </div>
</div>

@endif

</x-layout>