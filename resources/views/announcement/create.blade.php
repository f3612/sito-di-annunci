<x-layout>

    <div class="container-fluid mt-5 ">

        <div class="row justify-content-center align-items-center">
            <div class="col-12 text-center shadow">

                <h1 class="display-1 "> {{__('ui.addAdv')}} </h1>

            </div>
        </div>
    </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
        @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
        @endforeach
                </ul>
            </div>
        @endif


    <div class="container-fluid  bgCustom1">
        <div class="row justify-content-center align-items-center ">
            <div class="col-12 col-md-6 shadow p-4 bg-white my-3">
                <form method='POST' action="{{ route('announcement.store') }}">
                    @csrf
                    <input type="hidden" name='uniqueSecret'value='{{$uniqueSecret}}'>
                    <div class="mb-3">
                        <label for="exampleInputName" class="form-label">{{__('ui.prodotto')}}</label>
                        <input type="text" name='name' class="form-control" value="{{old('name')}}" id="exampleInputName" aria-describedby="emailHelp" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputBrand" class="form-label">{{__('ui.inserisci')}} Brand</label>
                        <input type="text" name='brand' class="form-control" value="{{old('brand')}}" id="exampleInputBrand" aria-describedby="emailHelp" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPrice" class="form-label">{{__('ui.insprezzo')}}</label>
                        <input type="number" name='price' class="form-control"  id="exampleInputPrice" aria-describedby="emailHelp" required>
                    </div>
                    <div class="mb-3">
                        <label for="Categorie">{{__('ui.sceglicategoria')}}</label>
                         <select name="category" id="Categorie">
                         @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                         @endforeach
                        </select>
                    </div>


                <div class="mb-3">
                    <label for="exampleInputdescription" class="form-label">{{__('ui.piccoladescr')}}</label>
                        <textarea name="description" class="form-control" cols="10" rows="6" id="exampleInputDescription" required>{{old('description')}}</textarea>
                </div>
                <div class='row'>
                    <label for="images" class='col-12 col-form-label'>{{__('ui.img')}} (Max:4)</label>
                    <div class="col-12">
                         <div class="dropzone" id='drophere'></div>
                    </div>
                </div>

                    <button type="submit" class="btn btn-dark brgold">{{__('ui.addAdv')}}</button>
                </form>
            </div>
        </div>
    </div>



</x-layout>