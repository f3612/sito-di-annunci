<x-layout>
  <div class="container">
    <div class="row mt-5">
      @foreach ($announcements as $announcement)
      <div class="col-12 col-lg-4 mt-5">
        <div class="card shadow" style="width: 18rem;">
            <div class="card-body">
              <h5 class="card-title fw-bold">{{$announcement->name}}</h5>
              <p class="prezzo">{{$announcement->price}}€</p>
              
              @foreach($announcement->images as $image)
                @if($loop->first)
                  <img id="card-first-img1" src="{{$image->getUrl(300,300)}}"
                  class="card-img-top" alt="immagine prodotto">
          
                @endif
                @if($loop->last)
                  <img id="card-last-img1" src="{{$image->getUrl(300,300)}}"
                  class="card-img-top" alt="immagine prodotto">
          
                @endif
              @endforeach
              
              <p class="card-text">{{$announcement->description}}</p>

              <p>{{__('ui.inseritoil')}}: {{$announcement->created_at->format('d/m/y')}}</p>
              <a href="{{ route('detailad',compact('announcement')) }}" class="btn btn-dark brgold">{{__('ui.scopri')}}</a>
            </div>
        </div>
      </div>
        @endforeach
      
    </div>
 </div>





</x-layout>