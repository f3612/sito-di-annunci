<x-layout>
    <div class="container mt-5">
        <div class="row">
            <div class="col-6 mt-5">
                    
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <?php $i=0; foreach ($announcement->images as $image): ?>
                            <?php if ($i==0) {$set_ = 'active'; } else {$set_ = ''; } ?> 
                              <div class='carousel-item <?php echo $set_; ?>'>
                                    <img src='{{$image->getUrl(300,300)}}' class='d-block w-100'>
                              </div>
                            <?php $i++; endforeach ?>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </button>
                      </div>


            </div>
            <div class="col-6 mt-5 bordcustom">
                <div>
                    <h3 class="txtcolor">{{$announcement->name}}</h3>
                    <h5 class="text-main ">{{$announcement->brand}}</h5>

                    <h4 class="text-main">{{$announcement->price}}€</h4>
                    <p>{{$announcement->description}}</p>
                    <hr>
                    <p class='text-muted'>{{__('ui.aggiuntoda')}} : {{$announcement->user->name}}</p>
                </div>
            </div>

        </div>
    </div>
</x-layout>