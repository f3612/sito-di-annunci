<x-layout>

@if (session('message'))
        <div class="alert alert-success position-absolute messaggi">
            {{ session('message') }}
        </div>
 @endif


{{-- messaggi di errore se non sei revisore --}}


@if(session('access.denied.revisor.only'))
 <div class="alert alert-danger position-absolute messaggi">
     {{__('ui.messnorevisore')}}
 </div>
 @endif
    


    <section class="header">
		<div class="bannerVideo" id="slideShow">
			<video src="/media/videooffice.mp4" autoplay muted loop class="active" ></video> 
			<video src="/media/videoann.mp4" autoplay muted loop ></video> 
			<video src="/media/video-6.mp4" autoplay muted loop></video> 
		</div>
		<div class="containerVideos container">
		
			<div class="content ">
				<div class="bannerText ms-3" id="slideShowText">
					<div class="active testoHeader">
						<h1 class='text-white display-1 text-shadow'>Presto</h1>

						<p class='subtext1'>{{__('ui.subText')}}</p>
                        <form action="{{route('search')}}" method="get">
                            @csrf 
                            <input type="text" name="q">
                            <button class="btn btn-dark brgold" type="submit">{{__('ui.cerca')}}</button>
                        </form>
					</div>
					<div class="testoHeader">
						<h1 class='text-white display-1 text-shadow'>{{__('ui.vendi')}}</h1>
                        <p class='subtext1'>{{__('ui.subText')}}</p>
                        @guest
                            <a href="{{route('login')}}" class="btn btn-dark brgold">{{__('ui.addAdv')}}</a>
                        @else
                            <a href="{{route('announcement.create')}}" class="btn btn-dark brgold">{{__('ui.addAdv')}}</a>
                         @endguest
					</div>
					<div class="testoHeader">
						<h1 class='text-white display-1 text-shadow'>{{__('ui.compra')}}</h1>
						<p class='subtext1'>{{__('ui.subText')}}</p>
                        <a href="{{route('announcement.index')}}" class="btn btn-dark brgold">{{__('ui.guardaann')}}</a>
					</div>
					
				</div>
			</div>
			
			<ul class="controls">
				<li class="rounded-circle bgCustom1"><img src="/media/fast-forward.png" class="rotazione" style="width: 20px" onclick="prevSlide();prevSlideText();"></li>
				<li class="rounded-circle ms-2 bgCustom1"><img src="/media/fast-forward.png" style="width: 20px" onclick="nextSlide();nextSlideText();"></li>
			</ul>
		</div>


        <script>
            const slideShow = document.getElementById('slideShow');
            const slides = slideShow.getElementsByTagName('video');
            var index = 0;
            function nextSlide(){
                slides[index].classList.remove('active');
                index = (index + 1) % slides.length;
                slides[index].classList.add('active');
            }
            function prevSlide(){
                slides[index].classList.remove('active');
                index = (index - 1 + slides.length) % slides.length;
                slides[index].classList.add('active');
            }
    
    
    
            const slideShowText = document.getElementById('slideShowText');
            const slidesText = slideShowText.getElementsByTagName('div');
            var i = 0;
            function nextSlideText(){
                slidesText[i].classList.remove('active');
                i = (i + 1) % slidesText.length;
                slidesText[i].classList.add('active');
            }
            function prevSlideText(){
                slidesText[i].classList.remove('active');
                i = (i - 1 + slidesText.length) % slidesText.length;
                slidesText[i].classList.add('active');
            }
    
            function toggleMenu(){
                const menuIcon = document.querySelector('.menuIcon');
                const navbar = document.getElementById('navbar');
                menuIcon.classList.toggle('active');
                navbar.classList.toggle('active');
            }
        </script>

        {{-- bottone scrolla giu --}}
    <div class='d-flex justify-content-end'>
        <a href="#" class=" text-decoration-none custum-button-top d-none d-flex justify-content-center align-items-center " id='bottonTop' > <i class="fas fa-arrow-up fa-2x txtcolor"></i></a>
    </div>


	</section>




    <div class="container my-5">
        <div class="row">
                <h2>{{__('ui.mostraCategorie')}}:</h2>
                @foreach($categories as $category)
                    <div class="col-12 col-lg-3 mt-5 ">
                        <div class="brgold bg-dark text-center p-2">
                            <p class="txtcolor h3"> {{ $category->name }}</p>
                            @if($category->name == "informatica")
                            <a href="{{route('announcements.category', [
                            $category->name,
                            $category->id,
                            ])}}"><i class="fas fa-laptop txtcolor fa-3x"></i></a>
                            
                            @elseif($category->name == "fotografia")
                            <a href="{{route('announcements.category', [
                            $category->name,
                            $category->id,
                            ])}}"><i class="fas fa-camera txtcolor fa-3x"></i></a>
                            
                            @elseif($category->name == "motori")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-car txtcolor fa-3x"></i></a>
                            
                            @elseif($category->name == "libri")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-book fa-3x txtcolor"></i></a>

                            @elseif($category->name == "collezionismo")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-chess-rook txtcolor fa-3x"></i></a>
                            
                            @elseif($category->name == "giardino e fai da te")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-tree txtcolor fa-3x"></i></a>

                            @elseif($category->name == "arredamento")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-couch txtcolor fa-3x"></i></a>

                            @elseif($category->name == "console")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-gamepad txtcolor fa-3x"></i></a>

                            @elseif($category->name == "appartamenti")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-home txtcolor fa-3x"></i></a>
                            
                            @elseif($category->name == "nautica")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-ship txtcolor fa-3x"></i></a>
                                @elseif($category->name == "giocattoli")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-volleyball-ball txtcolor fa-3x"></i></a>
                                @elseif($category->name == "elettrodomestici")
                            <a href="{{route('announcements.category', [
                                $category->name,
                                $category->id,
                                ])}}"><i class="fas fa-blender txtcolor fa-3x"></i></a>
                            @endif
                        </div>
                    </div>
                @endforeach
        </div>
    </div>









    
    <div style='margin-top:100px' > </div>
    
    
    <div class="container">
        <div class="row justify-content-center">
        
            @foreach ($announcements as $announcement)
            <div class="col-12 col-lg-4">
                
                <div class="card shadow my-3 brgold1 scalingcustom" id="cardtest" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title fw-bold">{{$announcement->name}}</h5>
                        <p class="prezzo">{{$announcement->price}}€</p>

                        
                        @foreach($announcement->images as $image)
                            @if($loop->first)
                                <img id="card-first-img" src="{{$image->getUrl(300,300)}}"
                                class="card-img-top" alt="immagine prodotto">
                        
                            @endif
                            @if($loop->last)
                                <img id="card-last-img" src="{{$image->getUrl(300,300)}}"
                                class="card-img-top" alt="immagine prodotto">
                        
                            @endif
                        @endforeach


                        <p class="card-text">{{$announcement->description}}</p>
                        <p>{{__('ui.inseritoil')}}: {{$announcement->created_at->format('d/m/y')}}</p>
                       
                          <strong> {{__('ui.category')}}: <a href="{{route('announcements.category', [
                            $announcement->category->name,
                            $announcement->category->id,
                            ])}}"> {{ $announcement->category->name}}</a></strong>
                            <div class="card-footer">

                                <i>{{__('ui.aggiuntoda')}}: {{ $announcement->user->name }}</i>
                                <a href="{{ route('detailad',compact('announcement')) }}" class="btn btn-dark brgold">{{__('ui.scopri')}}</a>
                            </div>
                       
                    </div>
                </div>
        
                  
            </div>
            @endforeach 
    </div>
</div>




</x-layout>