<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[PublicController::class, 'homepage'])->name('homepage');
Route::get('/login',[PublicController::class, 'login'])->name('login');
Route::get('/register',[PublicController::class, 'register'])->name('register');
Route::get('/category/{name}/{id}/announcements', [PublicController::class, 'announcementsByCategory'])->name('announcements.category');

//Rotte private
Route::get('/announcement/create', [AnnouncementController::class,'create'])->name('announcement.create');
Route::post('/annoucement/store',[AnnouncementController::class,'store'])->name('announcement.store');
Route::get('/announcement/index',[AnnouncementController::class,'index'])->name('announcement.index');
/* upload immagini */
Route::post('/announcement/images/upload', [AnnouncementController::class,'uploadimg'])->name('announcement.images.upload');
Route::delete('/announcement/images/remove', [AnnouncementController::class,'removeimg'])->name('announcement.images.remove');

//Rotta mail
Route::get('/contact' , [PublicController::class, 'formMail' ])->name('formMail');
Route::post('/send' , [PublicController::class, 'send' ])->name('mail.send');

//rotta lingue
Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');

//rotte revisore
Route::get('/revisor/index', [RevisorController::class, 'index'])->name('revisor.index');

Route::post('/revisor/announcement/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');

Route::post('/revisor/announcement/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');

Route::get('/announcement/detail/{announcement}',[AnnouncementController::class,'show'])->name('detailad');



Route::get('/search' , [PublicController::class, 'search'])->name('search');

