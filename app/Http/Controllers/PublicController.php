<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Mail\ContactMail;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{

    public function homepage(){

        $announcements= Announcement::where('is_accepted', true)->orderBy('created_at', 'desc')->take(5)->get();
        $categories = Category::all();
        $annunci= Announcement::where('is_accepted', true)->orderBy('created_at', 'desc')->get();


        return view('welcome', compact('announcements','categories'));

    }


    public function login(){
        return view('auth.login');
    }

    public function register(){
        return view('auth.register');
    }


    public function announcementsByCategory($name, $category_id){

       $category = Category::find($category_id);

       $announcements = $category->announcements()->where('is_accepted', true)->paginate(50);

       return view('announcement.category', compact('category', 'announcements'));

    }

    public function search(Request $request) {
            $q = $request->input('q');
            $announcements = Announcement::search($q)->where('is_accepted' , true)->get();
            return view('search_result' , compact('q' , 'announcements'));
    }

    public function formMail(){

        return view('mail.candidati');

    }
    public function send(Request $request){

        $name=$request->name;
        $email=$request->email;
        $description=$request->description;
        $userContact=compact('name', 'email' , 'description' );

        Mail::to($email)->send(new ContactMail($userContact));
        return redirect(route('homepage'))->with('message' , 'La tua candidatura è stata inviata correttamente' );

    }

//funzione lingue

    public function locale($locale){

        session()->put('locale', $locale);
        return redirect()->back();
    }

}
