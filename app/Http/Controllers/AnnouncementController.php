<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Jobs\ResizeImage;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements= Announcement::where('is_accepted', true)->orderBy("created_at",'DESC')->get();
        
        return view('announcement.index',compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::all();
        $uniqueSecret= base_convert(sha1(uniqid(mt_rand())),16, 36);
        return view('announcement.create', compact('categories','uniqueSecret'));
    }
    /* funzione caricamento immagine */

    public function uploadimg(Request $request){
        $uniqueSecret = $request-> input('uniqueSecret');
        $fileName = $request-> file('file')->store("public/temp/{$uniqueSecret}");
        
        // dispatch(new ResizeImage(
        //     $fileName,
        //     80,
        //     80
        // ));

        
        session()->push("images.{$uniqueSecret}",$fileName);
        
        return response()->json(

            ['id'=> $fileName]
        );

    }

    public function removeimg(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);

        return response()->json('ok');
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $announcements=Announcement::create([
            "name"=>$request->name,
            "brand"=>$request->brand,
            "price"=>$request->price,
            "description"=>$request->description,
            "category_id"=>$request->category,
            "user_id"=>Auth::user()->id,
        ]);

        $uniqueSecret = $request->input('uniqueSecret');

        $images= session()->get("images.{$uniqueSecret}", []);
        $removedImages= session()->get("removedimages.{$uniqueSecret}", []);
        
        $images=array_diff($images, $removedImages);

    

        foreach($images as $image){
            $i = new AnnouncementImage();
            
            $fileName = baseName($image);
            
            $newFileName = "public/storage/announcements/{$announcements->id}/{$fileName}";
            
            $file = Storage::move($image, $newFileName);
            


            $i->file = $newFileName;
            
            $i->announcement_id= $announcements->id;
            $i->save();


            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                new ResizeImage($i->file,300,300)
            ])->dispatch($i->id);

        }


        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));

        return redirect(route('homepage'))->with('message','hai aggiunto un annuncio con successo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        return view('announcement.detail',compact('announcement'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }
}
