<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use Illuminate\Http\Request;

class RevisorController extends Controller
{

    //middleware per fare in modo che questo controller funzioni solo con il revisore
    public function __construct()
    {
        
        $this->middleware('auth.revisor');
    }


    //funzione che serve per passare gli annunci nella vista del revisore (tutti gli annunci) 

    public function index(){
        
    $announcement = Announcement::where('is_accepted', null)->orderBy('created_at', 'desc')->first();

    return view('revisor.index', compact('announcement'));

    }


//funzione che serve per assegnare un valore true o false all'annucio
    
    private function setAccepted($announcement_id, $value){

        $announcement = Announcement::find($announcement_id);
        $announcement->is_accepted = $value;
        $announcement->save();

        return redirect(route('revisor.index'));
    }


//sono le funzioni per accettare o rifiutare  un annuncio e usano la funzione setAccepted() *creata sopra*

    public function accept($announcement_id){

        return $this->setAccepted($announcement_id, true);
    }

    public function reject($announcement_id){

        return $this->setAccepted($announcement_id, false);
    }

}
