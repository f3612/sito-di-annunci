<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $categories=[
            ['informatica'],
            ['motori'],
            ['fotografia'],
            ['libri'],
            ['collezionismo'],
            ['giardino e fai da te'],
            ['arredamento'],
            ['console'],
            ['appartamenti'],
            ['nautica'],
            ['giocattoli'],
            ['elettrodomestici'],

        ];

        foreach ($categories as $category){
            DB::table('categories')->insert([
                'name'=>$category[0],
            ]);
        }
    }
}
